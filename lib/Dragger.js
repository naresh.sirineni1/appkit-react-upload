'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var babelPluginFlowReactPropTypes_proptype_Node = require('react').babelPluginFlowReactPropTypes_proptype_Node || require('prop-types').any;

var babelPluginFlowReactPropTypes_proptype_CommonType = require('appkit-react-utils/commonType').babelPluginFlowReactPropTypes_proptype_CommonType || require('prop-types').any;

var Dragger = function (_Component) {
  _inherits(Dragger, _Component);

  function Dragger(props) {
    _classCallCheck(this, Dragger);

    var _this = _possibleConstructorReturn(this, (Dragger.__proto__ || Object.getPrototypeOf(Dragger)).call(this, props));

    _this.onFileDrop = function (e) {
      if (e.type === 'drop') {
        var files = Array.prototype.slice.call(e.dataTransfer.files);
        _this.props.onDragUpload(files);
      }
      e.preventDefault();
    };

    return _this;
  }

  _createClass(Dragger, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        {
          className: 'a-upload-drag-zone',
          onDrop: function onDrop(e) {
            return _this2.onFileDrop(e);
          },
          onDragOver: function onDragOver(e) {
            return _this2.onFileDrop(e);
          },
          onDragLeave: function onDragLeave(e) {
            return _this2.onFileDrop(e);
          }
        },
        this.props.children
      );
    }
  }]);

  return Dragger;
}(_react.Component);

Dragger.propTypes = {
  children: _propTypes2.default.node,
  onDragUpload: _propTypes2.default.func
};

Dragger.displayName = 'Dragger';

exports.default = Dragger;