'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _AjaxUpload = require('./AjaxUpload');

var _AjaxUpload2 = _interopRequireDefault(_AjaxUpload);

var _utils = require('./utils');

var _UploadList = require('./UploadList');

var _UploadList2 = _interopRequireDefault(_UploadList);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var babelPluginFlowReactPropTypes_proptype_Node = require('react').babelPluginFlowReactPropTypes_proptype_Node || require('prop-types').any;

var babelPluginFlowReactPropTypes_proptype_CommonType = require('appkit-react-utils/commonType').babelPluginFlowReactPropTypes_proptype_CommonType || require('prop-types').any;

var babelPluginFlowReactPropTypes_proptype_FileModel = require('./types').babelPluginFlowReactPropTypes_proptype_FileModel || require('prop-types').any;

var Upload = function (_Component) {
  _inherits(Upload, _Component);

  function Upload(props) {
    _classCallCheck(this, Upload);

    var _this = _possibleConstructorReturn(this, (Upload.__proto__ || Object.getPrototypeOf(Upload)).call(this, props));

    _this.onStart = function (file) {
      var fileList = _this.state.fileList;


      if (!_this.props.multiple) {
        fileList.splice(0);
      }
      fileList.push(file);
      _this.props.onChange(file, fileList);
      _this.setState({ fileList: fileList });
    };

    _this.onProgress = function (percent, file) {
      var fileList = _this.state.fileList;
      var targetItem = (0, _utils.getFileItem)(file, fileList);
      if (targetItem) {
        targetItem.status = 'uploading';
        targetItem.percent = percent;
        _this.props.onProgress(targetItem);
        _this.setState({ fileList: fileList });
      }
    };

    _this.onError = function (error, file) {
      var fileList = _this.state.fileList;
      var targetItem = (0, _utils.getFileItem)(file, fileList);
      if (targetItem) {
        targetItem.error = error;
        targetItem.status = 'error';
        _this.setState({ fileList: fileList }, function () {
          _this.props.onError(targetItem);
          _this.props.onChange(targetItem, fileList);
        });
      }
    };

    _this.onUnaccept = function (file) {
      var fileList = _this.state.fileList;


      file.error = "_unaccept_";
      file.status = 'error';

      if (!_this.props.multiple) {
        fileList = [];
      }

      fileList.push(file);
      _this.setState({ fileList: fileList });
    };

    _this.onSuccess = function (response, file) {
      response = (0, _utils.parseJson)(response);
      var fileList = _this.state.fileList;
      var targetItem = (0, _utils.getFileItem)(file, fileList);
      if (targetItem) {
        targetItem.status = 'success';
        targetItem.response = response;
        _this.setState({ fileList: fileList }, function () {
          _this.props.onSuccess(targetItem);
          _this.props.onChange(targetItem, fileList);
        });
      }
    };

    _this.handleRemoveFile = function (file) {
      var fileList = _this.state.fileList;
      var targetItem = (0, _utils.getFileItem)(file, fileList);

      //cancel request
      _this.uploadInner.cancel(file.uid);

      if (targetItem) {
        targetItem.status = "deleted";
        var newFileList = (0, _utils.removeFile)(file, fileList);
        _this.setState({ fileList: newFileList }, function () {
          return _this.props.onRemove(targetItem, newFileList);
        });
        _this.props.onChange(targetItem, newFileList);
      }
    };

    _this.submit = function () {
      _this.state.fileList.filter(function (file) {
        return file.status === 'ready';
      }).forEach(function (file) {
        _this.uploadInner.upload(file);
      });
    };

    _this.retryUpload = function (file) {
      if (file) {
        _this.uploadInner.upload(file);
      }
    };

    _this.renderUploadList = function () {
      return _react2.default.createElement(_UploadList2.default, {
        showFullFileName: _this.props.showFullFileName,
        files: _this.state.fileList,
        onRemove: _this.handleRemoveFile,
        autoUpload: _this.props.autoUpload,
        multiple: _this.props.multiple,
        retryUpload: _this.retryUpload
      });
    };

    _this.generateUploaderFragment = function () {
      var uploaderProps = {
        autoUpload: _this.props.autoUpload,
        action: _this.props.action,
        multiple: _this.props.multiple,
        beforeUpload: _this.props.beforeUpload,
        withCredentials: _this.props.withCredentials,
        headers: _this.props.headers,
        disabled: _this.props.disabled,
        data: _this.props.data,
        accept: _this.props.accept,
        drag: _this.props.drag,
        onStart: _this.onStart,
        onProgress: _this.onProgress,
        onSuccess: _this.onSuccess,
        onError: _this.onError,
        onUnaccept: _this.onUnaccept,
        onRemove: _this.handleRemoveFile,
        externalUpload: _this.props.externalUpload,
        externalCancel: _this.props.externalCancel,
        extraPropToFlowjs: _this.props.extraPropToFlowjs,
        ref: function ref(_ref) {
          return _this.uploadInner = _ref;
        }
      };

      var trigger = _this.props.children;

      return _react2.default.createElement(
        _AjaxUpload2.default,
        Object.assign({ key: 'AjaxUpload' }, uploaderProps),
        trigger
      );
    };

    _this.state = {
      fileList: []
    };
    return _this;
  }

  /**
   * @param {FileModel} file - extended file object
   */


  /**
   * @param {Object} e - progress information
   * @param {FileModel} file - extended file object
   */


  /**
   * @param {Error} error - error object
   * @param {FileModel} file - extended file object
   */


  /**
   * @param {Object} response - server response
   * @param {FileModel} file - extended file object
   */


  /**
   * Remove file call back funtion
   * @param {FileModel} file - extended file object
   */


  /**
   * API: Upload file on trigger
   */


  _createClass(Upload, [{
    key: 'cancel',


    /**
     *  API: cancel current upload
     * 
     */
    value: function cancel() {
      var _this2 = this;

      this.state.fileList.filter(function (file) {
        return file.status === 'uploading';
      }).forEach(function (file) {
        _this2.uploadInner.cancel(file.uid);
      });
    }

    /**
     * Retry upload
     * @param {FileModel} file - extended file object
     */

  }, {
    key: 'render',
    value: function render() {
      var fileList = this.state.fileList;

      var _props = this.props,
          showFileList = _props.showFileList,
          className = _props.className,
          style = _props.style,
          autoUpload = _props.autoUpload,
          multiple = _props.multiple,
          disabled = _props.disabled,
          theme = _props.theme,
          action = _props.action,
          headers = _props.headers,
          data = _props.data,
          withCredentials = _props.withCredentials,
          drag = _props.drag,
          accept = _props.accept,
          beforeUpload = _props.beforeUpload,
          onRemove = _props.onRemove,
          onProgress = _props.onProgress,
          onSuccess = _props.onSuccess,
          onError = _props.onError,
          onChange = _props.onChange,
          showFullFileName = _props.showFullFileName,
          externalUpload = _props.externalUpload,
          externalCancel = _props.externalCancel,
          extraPropToFlowjs = _props.extraPropToFlowjs,
          otherProps = _objectWithoutProperties(_props, ['showFileList', 'className', 'style', 'autoUpload', 'multiple', 'disabled', 'theme', 'action', 'headers', 'data', 'withCredentials', 'drag', 'accept', 'beforeUpload', 'onRemove', 'onProgress', 'onSuccess', 'onError', 'onChange', 'showFullFileName', 'externalUpload', 'externalCancel', 'extraPropToFlowjs']);

      var containerClass = (0, _classnames2.default)('a-upload', theme, className, {
        'upload-needs-trigger': !autoUpload,
        'disabled-upload': disabled
      });
      var innerClass = (0, _classnames2.default)('a-upload-box', {
        'single-upload': !multiple,
        'multi-upload': multiple
      });
      var uploadList = void 0,
          uploader = void 0;

      uploader = this.generateUploaderFragment();
      // Generate upload list section
      if (showFileList && fileList.length) {
        uploadList = this.renderUploadList();
      }

      return _react2.default.createElement(
        'div',
        Object.assign({ style: style, className: containerClass }, otherProps),
        _react2.default.createElement(
          'div',
          { className: innerClass },
          uploader,
          uploadList
        )
      );
    }
  }]);

  return Upload;
}(_react.Component);

Upload.defaultProps = {
  headers: {},
  theme: 'default',
  showFileList: true,
  autoUpload: true,
  drag: true,
  showFullFileName: false,
  onRemove: function onRemove() {},
  onProgress: function onProgress() {},
  onSuccess: function onSuccess() {},
  onError: function onError() {},
  onChange: function onChange() {}
};


Upload.propTypes = {
  style: _propTypes2.default.object,
  className: _propTypes2.default.string,
  theme: _propTypes2.default.oneOf(['default', 'white']),
  action: _propTypes2.default.string.isRequired,
  headers: _propTypes2.default.object,
  data: _propTypes2.default.object,
  multiple: _propTypes2.default.bool,
  withCredentials: _propTypes2.default.bool,
  showFileList: _propTypes2.default.bool,
  autoUpload: _propTypes2.default.bool,
  drag: _propTypes2.default.bool,
  disabled: _propTypes2.default.bool,
  accept: _propTypes2.default.func,
  beforeUpload: _propTypes2.default.func,
  onRemove: _propTypes2.default.func,
  onProgress: _propTypes2.default.func,
  onSuccess: _propTypes2.default.func,
  onError: _propTypes2.default.func,
  onChange: _propTypes2.default.func,
  externalUpload: _propTypes2.default.func,
  externalCancel: _propTypes2.default.func,
  extraPropToFlowjs: _propTypes2.default.object
};

Upload.displayName = 'Upload';

exports.default = Upload;