'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _appkitReact = require('appkit-react');

var _utils = require('./utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function isEnter(e) {
  return e.key === "Enter";
}

var UploadList = function (_Component) {
  _inherits(UploadList, _Component);

  function UploadList() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, UploadList);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = UploadList.__proto__ || Object.getPrototypeOf(UploadList)).call.apply(_ref, [this].concat(args))), _this), _this.removeFile = function (file) {
      var onRemove = _this.props.onRemove;

      if (onRemove) {
        file.status = 'deleted';
        onRemove(file);
      }
    }, _this.renderErrorContext = function (file) {
      var errorMsg = void 0;
      if (file.error === "_unaccept_") {
        errorMsg = file.name + " is not accepted to upload";
      } else {
        errorMsg = 'Failed to upload file "' + file.name + '"';
      }

      return _react2.default.createElement(
        'div',
        { className: 'a-upload-error a-upload-file-list-item', key: file.uid },
        _react2.default.createElement(
          'span',
          { className: 'a-upload-error-label-danger' },
          'Error'
        ),
        _react2.default.createElement(
          'span',
          { className: 'a-upload-error-message', title: errorMsg },
          errorMsg
        ),
        _react2.default.createElement(_appkitReact.KODIV, { className: 'appkiticon icon-refresh-outline', isSpan: true, onClick: function onClick() {
            return _this.retryUpload(file);
          }, onKeyDown: function onKeyDown(e) {
            return isEnter(e) && _this.retryUpload(file);
          } }),
        _react2.default.createElement(_appkitReact.KODIV, { className: 'appkiticon icon-circle-delete-outline', isSpan: true, onClick: function onClick() {
            return _this.removeFile(file);
          }, onKeyDown: function onKeyDown(e) {
            return isEnter(e) && _this.removeFile(file);
          } })
      );
    }, _this.renderSuccessContext = function (file) {
      return _react2.default.createElement(
        'div',
        { className: 'a-upload-success a-upload-file-list-item', key: file.uid },
        _this.getFileIconDiv(file),
        _react2.default.createElement(
          'div',
          { className: 'a-upload-file-span' },
          _this.getFileNameDiv(file),
          _react2.default.createElement(
            'div',
            { className: 'fileSize' },
            (0, _utils.getFormatFileSize)(file.size)
          )
        ),
        _react2.default.createElement(
          'span',
          { className: 'a-upload-date' },
          (0, _utils.dateFormat)(Date.now())
        ),
        _react2.default.createElement(_appkitReact.KODIV, { className: 'appkiticon icon-delete-outline', isSpan: true, onClick: function onClick() {
            return _this.removeFile(file);
          }, onKeyDown: function onKeyDown(e) {
            return isEnter(e) && _this.removeFile(file);
          } })
      );
    }, _this.renderUploadingContext = function (file, status) {
      var hasProgress = status === "uploading" && 'percent' in file;
      var loadingProgress = hasProgress && _react2.default.createElement(_appkitReact.Progress, { className: 'a-upload-progress-bar', key: 'progress', percent: file.percent });

      var cn = (0, _classnames2.default)("a-upload-progressing", "a-upload-file-list-item", status);

      return _react2.default.createElement(
        'div',
        { className: cn, key: file.uid },
        _this.getFileIconDiv(file),
        _this.getFileNameDiv(file),
        loadingProgress,
        _react2.default.createElement(_appkitReact.KODIV, { className: 'appkiticon icon-circle-delete-outline', isSpan: true, onClick: function onClick() {
            return _this.cancelUploading(file);
          }, onKeyDown: function onKeyDown(e) {
            return isEnter(e) && _this.cancelUploading(file);
          } })
      );
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(UploadList, [{
    key: 'retryUpload',
    value: function retryUpload(file) {
      var retryUpload = this.props.retryUpload;

      retryUpload(file);
    }
  }, {
    key: 'cancelUploading',
    value: function cancelUploading(file) {
      this.removeFile(file);
    }
  }, {
    key: 'getFileNameDiv',
    value: function getFileNameDiv(file) {
      return _react2.default.createElement(
        'span',
        { className: 'fileName', title: file.name },
        this.props.showFullFileName ? file.name : (0, _utils.getSimpleName)(file.name)
      );
    }
  }, {
    key: 'getFileIconDiv',
    value: function getFileIconDiv(file) {
      return _react2.default.createElement('span', { className: (0, _utils.getFileIcon)(file.name) });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props$files = this.props.files,
          files = _props$files === undefined ? [] : _props$files;

      var classes = (0, _classnames2.default)("a-upload-file-list", {
        "multiple": this.props.multiple
      });

      var list = files.map(function (file) {
        var status = file.status;
        if (status === 'ready') {
          return _this2.renderUploadingContext(file, status);
        } else if (status === 'uploading') {
          return _this2.renderUploadingContext(file, status);
        } else if (status === 'error') {
          return _this2.renderErrorContext(file);
        } else if (status === 'success') {
          return _this2.renderSuccessContext(file);
        }
      });
      return _react2.default.createElement(
        'div',
        { className: classes },
        list
      );
    }
  }]);

  return UploadList;
}(_react.Component);

UploadList.propTypes = {
  style: _propTypes2.default.object,
  className: _propTypes2.default.string,
  onRemove: _propTypes2.default.func,
  autoUpload: _propTypes2.default.bool,
  multiple: _propTypes2.default.bool,
  retryUpload: _propTypes2.default.func,
  files: _propTypes2.default.any
};

UploadList.displayName = 'UploadList';

exports.default = UploadList;