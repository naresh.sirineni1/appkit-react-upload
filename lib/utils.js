'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getFileItem = getFileItem;
exports.removeFile = removeFile;
exports.generateUid = generateUid;
exports.dateFormat = dateFormat;
exports.getFileIcon = getFileIcon;
exports.getSimpleName = getSimpleName;
exports.getFormatFileSize = getFormatFileSize;
exports.parseJson = parseJson;
function getFileItem(file, fileList) {
  if (file) {
    var key = file.uid !== undefined ? 'uid' : 'name';
    return fileList.filter(function (item) {
      return item[key] === file[key];
    })[0];
  } else {
    return null;
  }
}

//file as fileModel
function removeFile(file, fileList) {
  var key = file.uid !== undefined ? 'uid' : 'name';
  var newFileList = fileList.filter(function (item) {
    return item[key] !== file[key];
  });
  if (newFileList.length === fileList.length) {
    return [];
  } else {
    return newFileList;
  }
}

var fileKey = 0;
function generateUid() {
  fileKey = fileKey % 100;
  return 'appkit-upload-' + Date.now() + '-' + ++fileKey;
}

function dateFormat(timestamp) {
  var dateString = '';
  if (timestamp) {
    var date = new Date(timestamp);

    var year = date.getFullYear();
    var day = date.getDate();
    var month = '';

    switch (date.getMonth()) {
      case 0:
        month = 'Jan';
        break;
      case 1:
        month = 'Feb';
        break;
      case 2:
        month = 'Mar';
        break;
      case 3:
        month = 'Apr';
        break;
      case 4:
        month = 'May';
        break;
      case 5:
        month = 'Jun';
        break;
      case 6:
        month = 'Jul';
        break;
      case 7:
        month = 'Aug';
        break;
      case 8:
        month = 'Sep';
        break;
      case 9:
        month = 'Oct';
        break;
      case 10:
        month = 'Nov';
        break;
      case 11:
        month = 'Dec';
        break;
    }
    dateString = month + ' ' + day + ',' + year;
  }
  return dateString;
}

function getFileIcon(fileFullName) {
  var flagIdx = fileFullName.lastIndexOf('.');
  var fileType = fileFullName.substring(flagIdx + 1);
  var iconType = 'file';
  if (fileType === 'pdf') {
    iconType = fileType;
  } else if (fileType === 'xlsx' || fileType === 'xls') {
    iconType = 'xls';
  } else if (fileType === 'docx' || fileType === 'doc') {
    iconType = 'doc';
  } else if (fileType === 'pptx' || fileType === 'ppt') {
    iconType = 'ppt';
  }

  var iconClass = 'a-upload-file-type-icon appkiticon icon-' + iconType + '-fill';
  return iconClass;
}

function getSimpleName(fileFullName) {
  var flagIdx = fileFullName.lastIndexOf('.');

  return fileFullName.substring(0, flagIdx);
}

function getFormatFileSize(size) {
  var fileSizeMB = (size / 1024 / 1024).toFixed(1);
  var fileSizeKB = (size / 1024).toFixed(1);
  var fileSize = void 0;
  if (parseFloat(fileSizeMB) < 1) {
    fileSize = fileSizeKB + 'KB';
  } else {
    fileSize = fileSizeMB + 'MB';
  }

  return fileSize;
}

function parseJson(obj) {
  try {
    if (typeof obj === 'string') {
      obj = JSON.parse(obj);
    }
  } catch (e) {
    //then is not a json format
    //just calm down
  } finally {
    return obj;
  }
}