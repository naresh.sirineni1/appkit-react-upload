'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Dragger = require('./Dragger');

var _Dragger2 = _interopRequireDefault(_Dragger);

var _utils = require('./utils');

var _flow = require('@flowjs/flow.js');

var _flow2 = _interopRequireDefault(_flow);

var _appkitReact = require('appkit-react');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var babelPluginFlowReactPropTypes_proptype_Node = require('react').babelPluginFlowReactPropTypes_proptype_Node || require('prop-types').any;

var babelPluginFlowReactPropTypes_proptype_CommonType = require('appkit-react-utils/commonType').babelPluginFlowReactPropTypes_proptype_CommonType || require('prop-types').any;

var babelPluginFlowReactPropTypes_proptype_FileModel = require('./types').babelPluginFlowReactPropTypes_proptype_FileModel || require('prop-types').any;

var AjaxUpload = function (_Component) {
  _inherits(AjaxUpload, _Component);

  function AjaxUpload(props) {
    _classCallCheck(this, AjaxUpload);

    var _this = _possibleConstructorReturn(this, (AjaxUpload.__proto__ || Object.getPrototypeOf(AjaxUpload)).call(this, props));

    _this.handleChange = function (e) {
      if (e.target instanceof HTMLInputElement) {
        var files = e.target.files;
        if (files) {
          _this.uploadFiles(files);
          _this.input.value = null;
        }
      }
    };

    _this.uploadFiles = function (files) {
      var _this$props = _this.props,
          multiple = _this$props.multiple,
          accept = _this$props.accept,
          onUnaccept = _this$props.onUnaccept;

      var postFiles = Array.prototype.slice.call(files);

      if (postFiles.length === 0) {
        return;
      }

      if (!multiple) {
        postFiles = postFiles.slice(0, 1);
      }

      var _postFiles = postFiles;

      if (accept) {
        _postFiles = accept(_postFiles);
      }

      if (_postFiles.length < postFiles.length) {
        var unacceptables = postFiles.filter(function (e) {
          return _postFiles.indexOf(e) === -1;
        });
        unacceptables.forEach(function (e) {
          onUnaccept(e);
        });
      }

      if (_postFiles.length === 0) {
        return;
      }

      _postFiles.forEach(function (file) {
        var fileModel = {
          uid: (0, _utils.generateUid)(),
          status: 'ready',
          percent: 0,
          name: file.name,
          size: file.size,
          type: file.type,
          lastModifiedDate: file.lastModifiedDate,
          originFile: file
        };

        _this.props.onStart(fileModel);
        if (_this.props.autoUpload) {
          _this.upload(fileModel);
        }
      });
    };

    _this.requestQueue = {}; //file to flow
    return _this;
  }

  _createClass(AjaxUpload, [{
    key: 'cancel',
    value: function cancel(uid) {
      this.requestQueue[uid] && this.requestQueue[uid].cancel();

      if (this.props.externalCancel) {
        this.props.externalCancel(uid);
      }
    }
  }, {
    key: 'upload',
    value: function upload(file) {
      var _this2 = this;

      var beforeUpload = this.props.beforeUpload;

      if (beforeUpload) {
        var beforeResult = beforeUpload(file);
        if (beforeResult && beforeResult.then) {
          beforeResult.then(function (processedFile) {
            if (Object.prototype.toString.call(processedFile) === '[object File]') {
              _this2.startUploading(processedFile);
            } else {
              _this2.startUploading(file);
            }
          });
        } else if (beforeResult === false && typeof this.props.onRemove === 'function') {
          this.props.onRemove(file);
        } else {
          this.startUploading(file);
        }
      } else {
        return this.startUploading(file);
      }
    }
  }, {
    key: 'startUploading',
    value: function startUploading(file) {
      var _props = this.props,
          headers = _props.headers,
          withCredentials = _props.withCredentials,
          data = _props.data,
          action = _props.action,
          onProgress = _props.onProgress,
          onSuccess = _props.onSuccess,
          onError = _props.onError,
          externalUpload = _props.externalUpload,
          extraPropToFlowjs = _props.extraPropToFlowjs;


      var fileAPI = _extends({
        headers: headers,
        query: data,
        target: action,
        withCredentials: withCredentials,
        singleFile: true,
        generateUniqueIdentifier: function generateUniqueIdentifier(file) {
          var d = new Date().toISOString();
          return d + "-" + file.name;
        }
      }, extraPropToFlowjs);

      if (externalUpload) {
        externalUpload(fileAPI, file);
        return;
      }

      var flow = new _flow2.default(fileAPI);
      var uid = file.uid;
      var that = this;

      flow.on('filesSubmitted', function () {
        flow.upload();
      });

      flow.on('fileSuccess', function (file, message) {
        onSuccess(message, file.file);
        delete that.requestQueue[uid];
      });

      flow.on('fileError', function (file, message) {
        file.file.uid = uid;
        onError(message, file.file);
      });

      flow.on('fileProgress', function (file) {
        var p = flow.progress() * 100;
        file.file.uid = uid;
        onProgress(p, file.file);
      });

      flow.addFile(file.originFile);

      this.requestQueue[uid] = flow;
    }
  }, {
    key: 'handleClick',
    value: function handleClick(e) {
      if (!this.props.disabled) {
        this.kodiv.disableOutlineForFileDialog();
        this.input.click();
      }
    }
  }, {
    key: 'onKeyDown',
    value: function onKeyDown(e) {
      var isEnter = e.key === "Enter";
      if (isEnter && !this.props.disabled) {
        this.input.click();
      }
    }
  }, {
    key: 'rendContext',
    value: function rendContext() {
      var _this3 = this;

      var _props2 = this.props,
          multiple = _props2.multiple,
          drag = _props2.drag,
          disabled = _props2.disabled;

      if (disabled) {
        return _react2.default.createElement(
          'div',
          { className: 'a-upload-ajax-context disabled', tabIndex: -1 },
          ' ',
          this.props.children,
          ' '
        );
      } else {
        var pureContext = _react2.default.createElement(
          _appkitReact.KODIV,
          { dragable: true, ref: function ref(_ref) {
              return _this3.kodiv = _ref;
            }, className: 'a-upload-ajax-context', tabIndex: 0,
            onClick: function onClick() {
              return _this3.handleClick();
            },
            onKeyDown: this.onKeyDown.bind(this) },
          this.props.children,
          _react2.default.createElement('input', {
            type: 'file',
            ref: function ref(node) {
              return _this3.input = node;
            },
            className: 'upload-input',
            onChange: function onChange(e) {
              return _this3.handleChange(e);
            },
            multiple: multiple,
            onClick: function onClick(e) {
              return e.stopPropagation();
            }
          })
        );

        if (drag) {
          pureContext = _react2.default.createElement(
            _Dragger2.default,
            { onDragUpload: this.uploadFiles },
            pureContext
          );
        }
        return pureContext;
      }
    }
  }, {
    key: 'render',
    value: function render() {
      return this.rendContext();
    }
  }]);

  return AjaxUpload;
}(_react.Component);

AjaxUpload.defaultProps = {
  disabled: false
};


AjaxUpload.propTypes = {
  accept: _propTypes2.default.func,
  action: _propTypes2.default.string.isRequired,
  autoUpload: _propTypes2.default.bool,
  drag: _propTypes2.default.bool,
  beforeUpload: _propTypes2.default.func,
  data: _propTypes2.default.object,
  headers: _propTypes2.default.object,
  multiple: _propTypes2.default.bool,
  onStart: _propTypes2.default.func,
  onProgress: _propTypes2.default.func,
  onSuccess: _propTypes2.default.func,
  onError: _propTypes2.default.func,
  onRemove: _propTypes2.default.func,
  withCredentials: _propTypes2.default.bool,
  disabled: _propTypes2.default.bool
};

AjaxUpload.displayName = 'AjaxUpload';

exports.default = AjaxUpload;