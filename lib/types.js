'use strict';

var babelPluginFlowReactPropTypes_proptype_UploadStatus = require('prop-types').oneOf(['uploading', 'error', 'deleted', 'success', 'ready']);
/**
 * File status
 */


/**
 * File Model
 */
if (typeof exports !== 'undefined') Object.defineProperty(exports, 'babelPluginFlowReactPropTypes_proptype_UploadStatus', {
  value: babelPluginFlowReactPropTypes_proptype_UploadStatus,
  configurable: true
});
var babelPluginFlowReactPropTypes_proptype_FileModel = {
  uid: require('prop-types').string.isRequired,
  type: require('prop-types').string.isRequired,
  name: require('prop-types').string.isRequired,
  size: require('prop-types').number.isRequired,
  lastModifiedDate: typeof Date === 'function' ? require('prop-types').instanceOf(Date) : require('prop-types').any,
  status: require('prop-types').oneOf(['uploading', 'error', 'deleted', 'success', 'ready']),
  percent: require('prop-types').number,
  originFile: typeof File === 'function' ? require('prop-types').instanceOf(File) : require('prop-types').any,
  response: require('prop-types').any,
  error: require('prop-types').any
};
if (typeof exports !== 'undefined') Object.defineProperty(exports, 'babelPluginFlowReactPropTypes_proptype_FileModel', {
  value: babelPluginFlowReactPropTypes_proptype_FileModel,
  configurable: true
});